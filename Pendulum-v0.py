import gym
from utils.experiment import experiment

ENV_NAME = "Pendulum-v0"
SCORING = ["rew", "cum_rew", "avg_cum_rew"][0]
seed = 2

env = gym.make(ENV_NAME)

if seed is not None:
    env.seed(seed)

dict_list = []

dict_list.append(
    {
        "name": "PPO",
        "layers_size": (128,),
        "lr": 3e-4,
        "batch_size": 64,
        "gamma": 0.9,
        "eps_clip": 0.2,
        "update_interval": 1000,
        "n_epochs": 10,
        "estimate_adv": "TD"
    }
)
dict_list.append(
    {
        "name": "DDPG",
        "layers_size": (32,),
        "lr_A": 1e-3,
        "lr_C": 1e-3,
        "gamma": 0.99,
        "max_memory": 1000,
        "batch_size": 100,
        "polyak": 0.995,
        "update_after": 20,
        "n_epochs": 1,
    }
)


grid_search=[]
n_runs = 1
n_ep = 40
seed = seed if n_runs == 1 else [s for s in range(seed, seed + n_runs)]

experiment(env, dict_list, n_runs=n_runs, n_ep=n_ep, env_name=ENV_NAME, scoring=SCORING, render=False, grid_search=grid_search, seed=seed)