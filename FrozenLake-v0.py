import gym
from utils.experiment import experiment

ENV_NAME = "FrozenLake-v0"
SCORING = ["rew", "cum_rew", "avg_cum_rew"][2]
seed = 0

env = gym.make(ENV_NAME, is_slippery=True)

ns = env.observation_space.n
na = env.action_space.n

dict_list = []
dict_list.append(
    {
        "name": "Q-learning",
        "ns": ns,
        "na": na,
        "epsilon": 0.1,
        "alpha": 0.2,
        "gamma": 0.99,
    }
)
# dict_list.append(
#     {
#         "name": "Double Q-learning",
#         "ns": ns,
#         "na": na,
#         "epsilon": 0.1,
#         "alpha": 0.2,
#         "gamma": 0.99,
#     }
# )
dict_list.append(
    {
        "name": "Sarsa",
        "ns": ns,
        "na": na,
        "epsilon": 0.1,
        "alpha": 0.2,
        "gamma": 0.99,
        "lambd": 0,
    }
)
dict_list.append(
    {
        "name": "Sarsa",
        "ns": ns,
        "na": na,
        "epsilon": 0.1,
        "alpha": 0.2,
        "gamma": 0.99,
        "lambd": 0.9,
    }
)
# dict_list.append(
#     {
#         "name": "Sarsa",
#         "ns": ns,
#         "na": na,
#         "epsilon": 0.1,
#         "alpha": 0.2,
#         "gamma": 0.99,
#         "lambda": 1,
#     }
# )

grid_search=[]
n_runs = 1
n_ep = 4000
seed = seed if n_runs == 1 else [s for s in range(seed, seed + n_runs)]

experiment(env, dict_list, n_runs=n_runs, n_ep=n_ep, env_name=ENV_NAME, scoring=SCORING, render=False, grid_search=grid_search, seed=seed)
