import numpy as np
import matplotlib.pyplot as plt
from scipy import stats
import tqdm
import matplotlib.pyplot as plt
from .dict2agent import dict2agent
from .env_agent_interaction import run_agent
import pandas as pd


def experiment(env, dict_list, n_runs, n_ep, env_name, scoring, render, grid_search, seed):
    progress_bar = True
    colors = ["b", "r", "g", "orange", "purple",  "darkcyan", "brown", "darkblue", "k"]
    if n_runs > 1:
        progress_bar = False
    q = stats.norm.ppf(0.975)
    plt.figure(0)
    for j, dic in enumerate(dict_list):
        col = colors[j]
        score = np.zeros((n_runs, n_ep))
        name = dic["name"]
        del dic["name"]
        print(name)
        print(dic)
        for i in tqdm.tqdm(range(n_runs)):
            seed_i = seed
            if isinstance(seed, list):
                seed_i = seed[i]
            agent = dict2agent(dic, name, seed_i, env.observation_space, env.action_space)
            score[i] = run_agent(env, agent, n_ep, progress_bar=progress_bar, render=render)
        print()
        ylab = "reward"
        if scoring == "cum_rew":
            score = np.cumsum(score, axis=1)
            ylab = "cumulative reward"
        elif scoring == "avg_cum_rew":
            score = np.cumsum(score, axis=1) / np.arange(1, n_ep + 1)
            ylab = "cumulated reward averaged over episodes"
        avg = np.mean(score, axis=0)
        std = np.std(score, axis=0)
        name = agent.name
        for para in grid_search:
            if para in dic.keys():
                val = dic[para]
                name = name + " - " + para + ": " + str(val)
        if scoring == "runs":
            plt.plot(range(n_ep), score[0], label=name, color=col)
            for i in range(1, score.shape[0]):
                plt.plot(range(n_ep), score[i], color=col)
        else:
            plt.plot(range(n_ep), avg, label=name, alpha=0.35, color=col)
            if scoring == "rew":
                smoothed_avg = pd.Series(avg).rolling(10).mean()
                plt.plot(range(n_ep), smoothed_avg, color=col)
        plt.fill_between(
            range(n_ep),
            avg - (q / np.sqrt(n_runs)) * std,
            avg + (q / np.sqrt(n_runs)) * std,
            alpha=0.1,
            color=col,
        )
    ax = plt.gca()
    ax.set_facecolor("0.8")
    plt.grid(color='white', linewidth=0.35)
    plt.xlabel("episode")
    plt.ylabel(ylab)
    plt.suptitle(env_name)
    plt.legend()
    plt.show()
