import torch.nn as nn
import torch
import torch.nn.functional as F
from torch.distributions import Categorical, Normal
from gym.spaces import Discrete, Box
import numpy as np
import scipy.signal


class Q_Net(nn.Module):
    def __init__(self, ds, na, n_hidden, layer_size):
        super(Q_Net, self).__init__()
        self.input_layer = nn.Linear(ds, layer_size)
        self.output_layer = nn.Linear(layer_size, na)
        self.hidden_layers = [nn.Linear(layer_size, layer_size) for _ in range(n_hidden)]

    def forward(self, x):
        output = F.relu(self.input_layer(x))
        for layer in self.hidden_layers:
            output = F.relu(layer(output))
        output = self.output_layer(output)
        return output

class DiscreteMLPQFunction(nn.Module):
    def __init__(self, ds, da, hidden_sizes, activation):
        super().__init__()
        self.q = mlp([ds] + list(hidden_sizes) + [da], activation)

    def forward(self, obs):
        q = self.q(obs)
        return q

######################################


def mlp(sizes, activation, output_activation=nn.Identity):
    layers = []
    for j in range(len(sizes)-1):
        act = activation if j < len(sizes)-2 else output_activation
        layers += [nn.Linear(sizes[j], sizes[j+1]), act()]
    return nn.Sequential(*layers)

class Actor(nn.Module):

    def expectation(self, s):
        s = torch.FloatTensor(s)
        pi = self.distribution(s)
        return pi.mean

    def log_prob(self, pi, a):
        raise NotImplementedError
        
    def expectation(self):
        raise NotImplementedError

    def forward(self, s, a=None):
        pi = self.distribution(s)
        log_proba = None
        if a is not None:
            if isinstance(self, GaussianMLPActor):
                a = a.unsqueeze(1)
            log_proba = self.log_prob(pi, a)
        return pi, log_proba

class GaussianMLPActor(Actor):
    def __init__(self, ds, da, act_lim, hidden_sizes, activation):
        super().__init__()
        pi_sizes = [ds] + list(hidden_sizes) + [da]
        self.mu = mlp(pi_sizes, activation, nn.Tanh)
        log_std = -0. * np.ones(da, dtype=np.float32)
        self.log_std = torch.nn.Parameter(torch.as_tensor(log_std))
        self.act_lim = act_lim
    
    def distribution(self, s):
        std = torch.exp(self.log_std)
        mu = self.mean(s)
        return Normal(mu, std)
    
    def log_prob(self, pi, a):
        return pi.log_prob(a).sum(axis=-1)
    
    def mean(self, s):
        return self.mu(s) * self.act_lim
    

class CategoricalMLPActor(Actor):
    def __init__(self, ds, da, hidden_sizes, activation):
        super().__init__()
        pi_sizes = [ds] + list(hidden_sizes) + [da]
        self.logits = mlp(pi_sizes, activation)
    
    def distribution(self, s):
        return Categorical(F.softmax(self.logits(s), dim=-1))

    def log_prob(self, pi, a):
        return pi.log_prob(a)
    
    def mean(self, s):
        return self.distribution(s).mean

class MLPQFunction(nn.Module):
    def __init__(self, ds, da, hidden_sizes, activation):
        super().__init__()
        self.q = mlp([ds + da] + list(hidden_sizes) + [1], activation)

    def forward(self, obs, act):
        q = self.q(torch.cat([obs, act], dim=-1))
        return torch.squeeze(q, -1)

class MLPVFunction(nn.Module):
    def __init__(self, ds, da, hidden_sizes, activation=nn.ReLU):
        super().__init__()
        self.v = mlp([ds] + list(hidden_sizes) + [1], activation)

    def forward(self, obs):
        v = self.v(obs)
        return torch.squeeze(v, -1)

def make_actor(ds, da, act_space, hidden_sizes, activation=nn.ReLU):
    if isinstance(act_space, Discrete):
        pi = CategoricalMLPActor(ds, da, hidden_sizes, activation)
    else:
        a_sup = act_space.high[0]
        pi = GaussianMLPActor(ds, da, a_sup, hidden_sizes, activation)
    return pi

class MLPActorCritic(nn.Module):
    def __init__(self, ds, da, act_space, hidden_sizes=(256, 256),
                 activation=nn.ReLU):
        super().__init__()

        if isinstance(act_space, Box):
            self.a_inf = act_space.low[0]
            self.a_sup = act_space.high[0]
        elif isinstance(act_space, Discrete):
            self.a_inf = -1
            self.a_sup = act_space.n

        self.pi = make_actor(ds, da, act_space, hidden_sizes, activation)
        self.q = MLPQFunction(ds, da, hidden_sizes, activation)
        self.v = MLPVFunction(ds, da, hidden_sizes, activation)
        

    def step(self, s):
        with torch.no_grad():
            dist = self.pi.distribution(s)
            a = torch.clamp(dist.sample(), self.a_inf, self.a_sup)
            log_proba = self.pi.log_prob(dist, a)
            value = self.v(s)
        return a, log_proba, value

    def act(self, s):
        with torch.no_grad():
            dist = self.pi.distribution(s)
            a = torch.clamp(dist.sample(), self.a_inf, self.a_sup)
            return a

# class ConvNet(nn.Module):
#     def __init__(self):
#         super(ConvNet, self).__init__()
#         self.conv1 = nn.Conv1d(1, 16, kernel_size=4, stride=1)
#         self.pool1 = nn.MaxPool1d(4)
#         self.conv2 = nn.Conv1d(16, 32, kernel_size=4, stride=1)
#         self.pool2 = nn.MaxPool1d(4)
#         self.conv3 = nn.Conv1d(32, 32, kernel_size=4, stride=1)
#         self.pool3 = nn.MaxPool1d(3)

#         self.fc1 = nn.Linear(384,100)
#         self.fc2 = nn.Linear(100, 40)
#         self.fc3 = nn.Linear(40, 1)
#         self.out_act = nn.Sigmoid()

#     def forward(self, x):
#         x = F.relu(self.pool1(self.conv1(x)))
#         x = F.relu(self.pool2(self.conv2(x)))
#         x = x.view(-1, self.num_flat_features(x))
#         x = F.relu(self.fc1(x))
#         x = F.relu(self.fc2(x))
#         y = self.out_act(self.fc3(x))
#         return y
