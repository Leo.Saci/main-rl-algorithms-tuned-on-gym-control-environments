from policies.Qlearning import qlearning
from policies.DoubleQlearning import double_qlearning
from policies.sarsa import Sarsa
from policies.sarsa_approximation import Sarsa_approximation
from policies.REINFORCE import REINFORCE
from policies.DeepREINFORCE import DeepREINFORCE
from policies.DeepAdvantageREINFORCE import DeepAdvantageREINFORCE
from policies.DQN import DQN
from policies.DoubleDQN import DoubleDQN
from policies.QAC import QAC
from policies.A2C import A2C
from policies.PPO import PPO
from policies.DDPG import DDPG


def dict2agent(dic, name, seed, obs_space, act_space):
    if name == "Q-learning":
        agent = qlearning(**dic, seed=seed)
    
    elif name == "Double Q-learning":
        agent = double_qlearning(**dic)

    elif name == "Sarsa":
        agent = Sarsa(**dic)
    
    elif name == "Sarsa approximation":
        agent = Sarsa_approximation(obs_space, act_space, **dic)
    
    elif name == "REINFORCE":
        agent = REINFORCE(obs_space, act_space, **dic, seed=seed)
        
    elif name == "Deep REINFORCE":
        agent = DeepREINFORCE(obs_space, act_space, **dic, seed=seed)
    
    elif name == "Deep Advantage REINFORCE":
        agent = DeepAdvantageREINFORCE(obs_space, act_space, **dic, seed=seed)

    elif name == "DQN":
        agent = DQN(obs_space, act_space, **dic, seed=seed)
    
    elif name == "Double DQN":
        agent = DoubleDQN(obs_space, act_space, **dic, seed=seed)
    
    elif name == "QAC":
        agent = QAC(obs_space, act_space, **dic, seed=seed)
    
    elif name == "A2C":
        agent = A2C(obs_space, act_space, **dic, seed=seed)

    elif name == "PPO":
        agent = PPO(obs_space, act_space, **dic, seed=seed)
    
    elif name == "DDPG":
        agent = DDPG(obs_space, act_space, **dic, seed=seed)
    return agent