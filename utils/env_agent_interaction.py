import numpy as np
from gym.spaces import Discrete, Box
import tqdm
import matplotlib.pyplot as plt


def run_agent(environment, agent, n_ep, progress_bar, render):
    reward_per_episod = []
    cumulative_rew = 0
    if progress_bar:
        episode_range = tqdm.trange(n_ep, unit="Episode", desc="Episodes")
    else:
        episode_range = range(n_ep)

    for ep in episode_range:
        R = 0
        s = environment.reset()
        a = agent.choose(s)
        power, l = 0, 0
        done = False
        while not done:
            if render and ep in [0, n_ep-1]:
                environment.render()
            if isinstance(environment.action_space, Box):
                a = [a]
            s_n, rew, done, info = environment.step(a)
            if isinstance(environment.action_space, Box):
                a = a[0]
            power += np.abs(a)
            l += 1
            R += rew
            a = agent.step(s, a, rew, s_n, done)
            s = s_n
        reward_per_episod.append(R)
        cumulative_rew += R
        if progress_bar:
            # episode_range.set_postfix({"Succes rate": cumulative_rew / (ep + 1)})
            episode_range.set_postfix({"Ep. reward": R})
        environment.close()
    return reward_per_episod


def plot(environment, agent, n_ep, render=False):
    reward_per_episod = run_agent(environment, agent, n_ep, render=render)
    cumulative_rew = np.cumsum(reward_per_episod)
    plt.figure()
    plt.plot(np.arange(1, n_ep + 1, 1), np.array(cumulative_rew), label=agent.name)
    plt.xlabel("Episodes")
    plt.ylabel("Cumulative reward")
    plt.grid()
    plt.legend()
    plt.show()
