import numpy as np
import random as rd

class ReplayMemory:
    def __init__(self, max_memory, batch_size):
        self.max_memory = max_memory
        self.batch_size = batch_size
        self.D = []

    def __len__(self):
        return len(self.D)

    def __reset__(self):
        self.D = []

    def add(self, data):
        if len(self.D) >= self.max_memory:
            self.D = self.D[1:]
        self.D.append(data)

    def sample(self):
        if len(self) < self.batch_size:
            batch = self.D
        else:
            batch = rd.sample(self.D, self.batch_size)
        return np.array(batch)