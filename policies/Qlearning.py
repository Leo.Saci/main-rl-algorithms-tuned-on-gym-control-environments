import numpy as np
import matplotlib.pyplot as plt

class qlearning:
    name = "Q-learning"

    def __init__(self, ns, na, epsilon, alpha, gamma, seed):
        if seed is not None:
            np.random.seed(seed)
        self.ns = ns
        self.na = na
        self.alpha = alpha
        self.epsilon = epsilon
        self.Q = np.zeros((ns, na))
        self.gamma = gamma

    def greedy(self, s):
        ind = np.where(self.Q[s] == self.Q[s].max())[0]
        return np.random.choice(ind)

    def eps_greedy(self, s):
        if np.random.rand() > self.epsilon:
            return self.greedy(s)
        else:
            return np.random.randint(self.na)

    def choose(self, s):
        a = self.eps_greedy(s)
        return a

    def step(self, s, a, rew, s_n, done):
        td = rew + self.gamma * np.max(self.Q[s_n]) - self.Q[s, a]
        self.Q[s, a] = self.Q[s, a] + self.alpha * td
        a_prime = self.choose(s_n)
        self.s = s_n
        self.a = a_prime
        return a_prime