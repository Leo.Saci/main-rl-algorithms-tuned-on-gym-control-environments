from gym.spaces import Discrete, Box
from copy import deepcopy
import numpy as np
import torch
import torch.nn as nn
from utils.nets import DiscreteMLPQFunction
from tensorboardX import SummaryWriter

class ReplayMemory:
    def __init__(self, max_memory):
        self.max_memory = max_memory
        self.__reset__()

    def __len__(self):
        return len(self.S)

    def __reset__(self):
        self.S = []
        self.A = []
        self.R = []
        self.S_N = []
        self.T = []

    def add(self, data):
        s, a, r, s_n, done = data[0], data[1], data[2], data[3], data[4]
        if len(self.S) >= self.max_memory:
            self.S = self.S[1:]
            self.A = self.A[1:]
            self.R = self.R[1:]
            self.S_N = self.S_N[1:]
            self.T = self.T[1:]
        self.S.append(s)
        self.A.append(int(a))
        self.R.append(r)
        self.S_N.append(s_n)
        self.T.append(done)

    def sample(self, idx):
        batch_s = torch.stack(self.S)[idx, :]
        batch_a = torch.tensor(self.A, dtype=torch.int64)[idx]
        batch_r = torch.FloatTensor(self.R)[idx]
        batch_s_n = torch.stack(self.S_N)[idx]
        batch_t = torch.tensor(self.T, dtype=torch.int)[idx]
        return batch_s, batch_a, batch_r, batch_s_n, batch_t



class DQN:
    name = "DQN"

    def __init__(self, obs_space, act_space, lr, max_memory, batch_size, epsilon, gamma, hidden_sizes, target_delay, seed):
        if seed is not None:
            torch.manual_seed(seed)

        if isinstance(obs_space, Box):
            ds = obs_space.shape[0]
        elif isinstance(obs_space, Discrete):
            ds = obs_space.n
        da = act_space.n
        self.ds = ds
        self.da = da

        self.lr = lr
        self.max_memory = max_memory
        self.batch_size = batch_size
        self.epsilon = epsilon
        self.gamma = gamma
        self.memory = ReplayMemory(max_memory=max_memory)
        self.timestep = 0

        self.model = DiscreteMLPQFunction(ds, da, hidden_sizes, nn.ReLU)
        self.optimizer = torch.optim.Adam(self.model.parameters(), lr=lr)
        
        self.target_Q = deepcopy(self.model)
        self.target_delay = target_delay

    def step(self, s, a, rew, s_n, done):
        self.timestep += 1
        data = (torch.FloatTensor(s), a, rew, torch.FloatTensor(s_n), done)
        self.memory.add(data)
        self.learn()
        
        if self.timestep % self.target_delay == 0:
            # self.target_Q.load_state_dict(self.model.state_dict())
            self.update_target_net()
        if done:
            return
        return self.choose(s_n)

    def choose(self, s):
        a = self.eps_greedy(s)
        return a

    def greedy(self, s):
        s = torch.tensor(s, dtype=torch.float32)
        return self.model(s).argmax().item()

    def eps_greedy(self, s):
        if np.random.rand() > self.epsilon:
            a = self.greedy(s)
        else:
            a = np.random.randint(self.da)
        return a

    def learn(self):
        if len(self.memory) < self.batch_size:
            return
        idx = list(range(self.memory.__len__()))
        np.random.shuffle(idx)
        idx = idx[:self.batch_size]
        batch_s, batch_a, batch_r, batch_s_n, batch_t = self.memory.sample(idx)
        max_q_values = self.target_Q(batch_s_n).max(1)[0]
        targets = batch_r + (1 - batch_t) * (self.gamma * max_q_values)
        batch_a = batch_a.unsqueeze(1)
        current_Q = self.model(batch_s).gather(1, batch_a)
        current_Q = current_Q.squeeze(1)

        loss = torch.nn.MSELoss()(current_Q, targets)
        
        self.optimizer.zero_grad()
        loss.backward()
        self.optimizer.step()
    
    def update_target_net(self):
        tau = 0
        with torch.no_grad():
            for p, p_targ in zip(self.model.parameters(), self.target_Q.parameters()):
                p_targ.data.mul_(tau)
                p_targ.data.add_((1 - tau) * p.data)