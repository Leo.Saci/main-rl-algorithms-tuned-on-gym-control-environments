from gym.spaces import Discrete, Box
import numpy as np
import gym

class REINFORCE:
    name = "REINFORCE"

    def __init__(self, obs_space, act_space, lr, gamma, seed):
        if seed is not None:
            np.random.seed(seed)
        if isinstance(obs_space, Box):
            ds = obs_space.shape[0]
        elif isinstance(obs_space, Discrete):
            ds = obs_space.n
        da = act_space.n
        self.da = da
        self.ds = ds
        
        self.lr = lr
        self.gamma = gamma
        self.t = 0
        self.dphis = self.phi(np.zeros(ds)).shape[0]  ##### SOURCE DE PROBLEME
        self.theta = np.ones((da, self.dphis))
        self.list_state = []
        self.list_action = []
        self.list_reward = []

    def reset_episode(self):
        self.list_reward = []
        self.list_state = []
        self.list_action = []

    @staticmethod
    def phi(s):
        return s

    @staticmethod
    def softmax(x):
        return np.exp(x) / np.sum(np.exp(x))

    def model(self, s):
        return self.theta @ self.phi(s)

    def grad_log_pi(self, s, a):
        grad = -self.softmax(self.model(s)).reshape((self.da, 1)) @ self.phi(s).reshape(
            (1, self.dphis)
        )
        grad[a] += self.phi(s)
        return grad

    def update(self):
        G = self.discount_rewards()
        for s, a, v in zip(self.list_state, self.list_action, G):
            self.theta += self.lr * self.grad_log_pi(s, a) * v

    def discount_rewards(self):
        # calculate temporally adjusted, discounted rewards
        rewards = self.list_reward
        discounted_rewards = np.zeros(len(rewards))
        cumulative_rewards = 0
        for i in reversed(range(0, len(rewards))):
            cumulative_rewards = cumulative_rewards * self.gamma + rewards[i]
            discounted_rewards[i] = cumulative_rewards
        return discounted_rewards

    def choose(self, s):
        probs = self.softmax(self.model(s))
        if np.isnan(probs.any()):
            raise NameError("Attention, il y a un NaN")
        #  return np.random.multivariate_normal(self.mean(s), self.cov)
        sample = np.random.multinomial(1, probs)
        return np.where(sample)[0][0]

    def step(self, s, a, rew, s_n, done):
        if not done:
            self.list_state.append(s)
            self.list_action.append(a)
            self.list_reward.append(rew)
            return self.choose(s_n)

        else:
            self.update()
            self.reset_episode()
            return