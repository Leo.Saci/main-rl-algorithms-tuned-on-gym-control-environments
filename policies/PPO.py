from gym.spaces import Discrete, Box
import torch
import torch.nn as nn
import torch.optim as optim
import torch.nn.functional as F
from torch.utils.data.sampler import BatchSampler, SubsetRandomSampler
from utils.nets import MLPActorCritic
from tensorboardX import SummaryWriter
import numpy as np

writer = SummaryWriter()


class Memory:
    def __init__(self, batch_size):
        self.batch_size = batch_size
        self.__reset__()

    def __len__(self):
        return len(self.S)

    def __reset__(self):
        self.S = []
        self.A = []
        self.R = []
        self.S_N = []
        self.T = []
        self.log_probs = []
    
    def add(self, data):
        s, a, r, s_n, done = data
        self.S.append(s)
        self.A.append(a)
        self.R.append(r)
        self.S_N.append(s_n)
        self.T.append(done)

        
class PPO:
    name = "PPO"
    def __init__(self, obs_space, act_space, layers_size, lr, batch_size, gamma, eps_clip, update_interval, n_epochs, estimate_adv, seed):
        if seed is not None:
            torch.manual_seed(seed)
        if isinstance(obs_space, Box):
            ds = obs_space.shape[0]
        elif isinstance(obs_space, Discrete):
            ds = obs_space.n
        if isinstance(act_space, Box):
            da = act_space.shape[0]
        elif isinstance(act_space, Discrete):
            da = act_space.n
        
        self.batch_size = batch_size
        self.gamma = gamma
        self.eps_clip = eps_clip

        self.update_interval = update_interval
        self.n_epochs = n_epochs

        self.ac = MLPActorCritic(ds, da, act_space, layers_size)

        self.optimizer_A = optim.Adam(self.ac.pi.parameters(), lr=lr)
        self.optimizer_C = optim.Adam(self.ac.v.parameters(), lr=lr)

        self.buffer = Memory(batch_size)

        self.timestep = 0
        self.i_epoch = 0

        self.estimate_adv = estimate_adv
    
    
    def step(self, s, a, rew, s_n, done):
        self.timestep += 1
        data = (torch.FloatTensor(s), a, rew, torch.FloatTensor(s_n), done)
        self.buffer.add(data)

        if self.update_interval is None:
            if done:
                self.finish_rollout()
                self.update_model()
                self.buffer.__reset__()

        elif self.timestep % self.update_interval == 0:
            self.finish_rollout()
            self.update_model()
            self.buffer.__reset__()
        
        if done:
            return
        else:
            a_n = self.choose(s_n)
            return a_n
    
    def choose(self, s):
        state = torch.FloatTensor(s)
        a, log_proba, _ = self.ac.step(state)
        a = a.item()
        self.buffer.log_probs.append(log_proba)
        return a
    
    def update_model(self):
        for _ in range(self.n_epochs):
            self.i_epoch += 1
            for idx in BatchSampler(SubsetRandomSampler(range(len(self.buffer))), self.batch_size, False):
                batch_s = self.S[idx]
                batch_a = self.A[idx]

                old_log_probs = self.log_probs[idx]
                batch_g = self.G[idx]
                batch_adv = self.adv[idx]
                batch_td = self.td_target[idx]

                _, new_log_probs = self.ac.pi(batch_s, batch_a)

                new_values = self.ac.v(batch_s)
                ratio = torch.exp(new_log_probs - old_log_probs.detach())
                
                surr1 = ratio * batch_adv.detach()
                surr2 = torch.clamp(ratio, 1 - self.eps_clip, 1 + self.eps_clip) * batch_adv.detach()

                actor_loss = - torch.min(surr1, surr2).mean()
                critic_loss = (batch_td.detach() - new_values).pow(2).mean()

                writer.add_scalar('actor loss', actor_loss, self.i_epoch)
                writer.add_scalar('critic loss', critic_loss, self.i_epoch)
                
                self.optimizer_A.zero_grad()
                actor_loss.backward()
                self.optimizer_A.step()

                self.optimizer_C.zero_grad()
                critic_loss.backward()
                self.optimizer_C.step()

    def finish_rollout(self):
        with torch.no_grad():
            GAE_target = []
            G = []
            tau = 0.95
            S = torch.stack(self.buffer.S)
            A = torch.FloatTensor(self.buffer.A)
            R = torch.FloatTensor(self.buffer.R)
            S_N = torch.stack(self.buffer.S_N)
            T = torch.tensor(self.buffer.T, dtype=torch.int)
            log_probs = torch.FloatTensor(self.buffer.log_probs)
            
            V = self.ac.v(S)
            V_N = (1 - T) * self.ac.v(S_N)
            
            self.td_target = R + self.gamma * V_N
            td_target_numpy = self.td_target.detach().numpy()

            gae_target, g = 0, 0
            for step in reversed(range(len(self.buffer))):
                gae_target = td_target_numpy[step] + self.gamma * tau * (1 - T[step]) * gae_target
                g = td_target_numpy[step] + self.gamma * (1 - T[step]) * g
                GAE_target.insert(0, gae_target + V[step])
                G.insert(0, g + V[step])
            self.S = S
            self.A = A
            self.R = R 
            self.S_N = S_N
            self.log_probs = log_probs
            self.GAE_target = torch.FloatTensor(GAE_target)
            self.G = torch.FloatTensor(G)

            if self.estimate_adv == "RTG":
                adv = self.G - V
            elif self.estimate_adv == "GAE":
                adv = self.GAE_target - V
            elif self.estimate_adv == "TD":
                adv = self.td_target - V
            self.adv = (adv - adv.mean()) / (adv.std() + 1e-9)

    def normalize(self, tens):
        return (tens - tens.mean()) / (tens.std() + 1e-9)






