import numpy as np
from gym.spaces import Discrete, Box
import torch
import torch.nn as nn
import torch.optim as optim
from torch.utils.data.sampler import BatchSampler, SubsetRandomSampler
from utils.nets import MLPActorCritic
import random as rd
from copy import deepcopy
from tensorboardX import SummaryWriter

writer = SummaryWriter()


class ReplayMemory:
    def __init__(self, max_memory):
        self.max_memory = max_memory
        self.__reset__()

    def __len__(self):
        return len(self.S)

    def __reset__(self):
        self.S = []
        self.A = []
        self.R = []
        self.S_N = []
        self.T = []

    def add(self, data):
        s, a, r, s_n, done = data[0], data[1], data[2], data[3], data[4]
        if len(self.S) >= self.max_memory:
            self.S = self.S[1:]
            self.A = self.A[1:]
            self.R = self.R[1:]
            self.S_N = self.S_N[1:]
            self.T = self.T[1:]
        self.S.append(s)
        self.A.append(a)
        self.R.append(r)
        self.S_N.append(s_n)
        self.T.append(done)

    def sample(self, idx):
        batch_s = torch.stack(self.S)[idx, :]
        batch_a = torch.FloatTensor(self.A)[idx]
        batch_r = torch.FloatTensor(self.R)[idx]
        batch_s_n = torch.stack(self.S_N)[idx]
        batch_t = torch.tensor(self.T, dtype=torch.int)[idx]
        return batch_s, batch_a, batch_r, batch_s_n, batch_t



class DDPG:
    name = "DDPG"
    def __init__(self, obs_space, act_space, layers_size, lr_A, lr_C, gamma, max_memory, batch_size, polyak, update_after, n_epochs, seed):
        if seed is not None:
            torch.manual_seed(seed)
        if isinstance(obs_space, Box):
            ds = obs_space.shape[0]
        elif isinstance(obs_space, Discrete):
            ds = obs_space.n
        da = act_space.shape[0]
        self.a_max = act_space.high[0]
        self.a_min = act_space.low[0]
        self.gamma = gamma
        self.polyak = polyak
        self.update_after = update_after
        self.n_epochs = n_epochs

        self.ac = MLPActorCritic(ds, da, act_space, layers_size)
        
        self.target_ac = deepcopy(self.ac)
        for p in self.target_ac.parameters():
            p.requires_grad = False
        
        self.batch_size = batch_size
        
        self.optimizer_A = optim.Adam(self.ac.pi.parameters(), lr=lr_A)
        self.optimizer_C = optim.Adam(self.ac.q.parameters(), lr=lr_C)

        self.buffer = ReplayMemory(max_memory)

        self.timestep = 0
        self.i_epoch = 0

    def step(self, s, a, rew, s_n, done):
        self.timestep += 1
        data = (torch.FloatTensor(s), a, rew, torch.FloatTensor(s_n), done)
        self.buffer.add(data)
        if self.update_after is None:
            if done and self.timestep > 10000:
                self.learn()
                self.update()
        elif self.timestep % self.update_after == 0 and self.timestep > 10000:
            self.learn()
            self.update()

        if done:
            return
        else:
            a_n = self.choose(s_n)
            return a_n
        
    def choose(self, s):
        if self.timestep < 10000:
            a = np.random.rand() * (self.a_max - self.a_min) + self.a_min
        else:
            state = torch.FloatTensor(s)
            action = self.ac.act(state)
            a = np.clip(action.item(), self.a_min, self.a_max)
        return a
    
    def learn(self):
        for _ in range(self.n_epochs):
            self.i_epoch += 1
            for idx in BatchSampler(SubsetRandomSampler(range(len(self.buffer))), self.batch_size, False):
                batch_s, batch_a, batch_r, batch_s_n, batch_t = self.buffer.sample(idx)
                
                with torch.no_grad():
                    max_actions = self.target_ac.pi.mean(batch_s_n)
                    targets = batch_r + (1 - batch_t) * self.gamma * self.target_ac.q(batch_s_n, max_actions)
                    # print(sum(1-batch_t))
                    # print(batch_r)
                    # print()
                batch_a = batch_a.unsqueeze(1)            
                q_values = self.ac.q(batch_s, batch_a)

                loss_C = (targets.detach() - q_values).pow(2).mean()
                self.optimizer_C.zero_grad()
                loss_C.backward()
                self.optimizer_C.step()

                max_actions = self.ac.pi.mu(batch_s)
                loss_A = - self.ac.q(batch_s.detach(), max_actions).mean()
                self.optimizer_A.zero_grad()
                loss_A.backward()
                self.optimizer_A.step()

                writer.add_scalar('actor loss', loss_A, self.i_epoch)
                writer.add_scalar('critic loss', loss_C, self.i_epoch)
        
    def update(self):
        with torch.no_grad():
            for p, p_targ in zip(self.ac.parameters(), self.target_ac.parameters()):
                p_targ.data.mul_(self.polyak)
                p_targ.data.add_((1 - self.polyak) * p.data)