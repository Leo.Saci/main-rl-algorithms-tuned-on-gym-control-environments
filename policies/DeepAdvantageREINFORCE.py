from gym.spaces import Discrete, Box
import torch
import torch.nn as nn
import torch.optim as optim
from utils.nets import make_actor, MLPVFunction


class DeepAdvantageREINFORCE:
    name = "Deep Advantage REINFORCE"
    def __init__(self, obs_space, act_space, lr_A, lr_V, gamma, hidden_sizes, seed):
        if seed is not None:
            torch.manual_seed(seed)
        if isinstance(obs_space, Box):
            ds = obs_space.shape[0]
        elif isinstance(obs_space, Discrete):
            ds = obs_space.n
        if isinstance(act_space, Box):
            da = act_space.shape[0]
        elif isinstance(act_space, Discrete):
            da = act_space.n
        
        self.gamma = gamma

        self.actor = make_actor(ds, da, act_space, hidden_sizes)
        self.value = MLPVFunction(ds, da, hidden_sizes)

        self.optimizer_A = optim.Adam(self.actor.parameters(), lr=lr_A)
        self.optimizer_V = optim.Adam(self.value.parameters(), lr=lr_V)
        
        self.list_reward = []
        self.values = []
        self.log_probas = []

    def reset_episode(self):
        self.list_reward = []
        self.values = []
        self.log_probas = []
    
    def step(self, s, a, rew, s_n, done):
        state = torch.FloatTensor(s)
        value = self.value(state)

        self.list_reward.append(torch.FloatTensor([rew]))
        self.values.append(value.unsqueeze(0))
        
        a = torch.tensor(a).unsqueeze(0)
        
        self.log_probas.append(self.pi.log_prob(a))

        if done:
            self.update()
            self.reset_episode()
            return
        else:
            a_n = self.choose(s_n)
            return a_n
    
    def choose(self, s):
        state = torch.FloatTensor(s)

        self.pi = self.actor.distribution(state)

        a = self.pi.sample().item()
        return a
    
    def compute_returns(self):
        rewards = self.list_reward
        rewards.reverse()
        G = [rewards[0]]
        G_t = G[0]
        for i in range(1, len(rewards)):
            r = rewards[i]
            G_t = r + self.gamma * G_t
            G.append(G_t)
        G.reverse()
        return G

    def update(self):
        G = self.compute_returns()
        G = torch.cat(G).detach()
        V = torch.cat(self.values)
        log_probas = torch.cat(self.log_probas)
        A = G - V
        loss_A = - (log_probas * A.detach()).mean()
        loss_V = A.pow(2).mean()
        
        self.optimizer_A.zero_grad()
        loss_A.backward()
        self.optimizer_A.step()

        self.optimizer_V.zero_grad()
        loss_V.backward()
        self.optimizer_V.step()