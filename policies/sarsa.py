import gym
import numpy as np 
import tqdm

class Sarsa:
    def __init__(self, ns, na, epsilon, alpha, gamma, lambd):
        self.name = 'Sarsa({})'.format(lambd)
        self.ns = ns
        self.na = na
        self.lambd = lambd
        self.alpha = alpha
        self.epsilon = epsilon
        self.E = np.zeros((ns, na))
        self.Q = np.zeros((ns, na))
        self.gamma = gamma
    
    def reset_episode(self):
        self.E = np.zeros((self.ns, self.na))
    
    def greedy(self, s):
        ind = np.where(self.Q[s] == self.Q[s].max())[0]
        return np.random.choice(ind)
    
    def eps_greedy(self, s):
        if np.random.rand() > self.epsilon:
            return self.greedy(s)
        else:
            return np.random.randint(self.na)
    
    def choose(self, s):
        a = self.eps_greedy(s)
        return a
    
    def step(self, s, a, rew, s_n, done):
        if done:
            self.reset_episode()
        a_prime = self.choose(s_n)
        self.a = a_prime
        self.s = s_n
        delta = rew + self.gamma * self.Q[s_n, a_prime] - self.Q[s, a]
        self.E = self.gamma * self.lambd * self.E
        self.E[s, a] = self.E[s, a] + 1
        self.Q = self.Q + self.alpha * delta * self.E
        return a_prime
