from gym.spaces import Discrete, Box
import torch
import torch.nn as nn
import torch.optim as optim
import torch.nn.functional as F
from torch.distributions import Categorical
from utils.nets import MLPActorCritic
from tensorboardX import SummaryWriter

writer = SummaryWriter()

class QAC:
    name = "QAC"
    def __init__(self, obs_space, act_space, layers_size, lr_A, lr_C, gamma, seed):
        if seed is not None:
            torch.manual_seed(seed)
        if isinstance(obs_space, Box):
            ds = obs_space.shape[0]
        elif isinstance(obs_space, Discrete):
            ds = obs_space.n
        if isinstance(act_space, Box):
            da = act_space.shape[0]
        elif isinstance(act_space, Discrete):
            da = act_space.n
        
        self.gamma = gamma
        self.ac = MLPActorCritic(ds, da, act_space, layers_size)

        self.optimizer_A = optim.Adam(self.ac.pi.parameters(), lr=lr_A)
        self.optimizer_C = optim.Adam(self.ac.v.parameters(), lr=lr_C)
        
        self.timestep = 0
    
    def step(self, s, a, rew, s_n, done):
        self.timestep += 1
        state = torch.FloatTensor(s)
        value = self.ac.v(state)
        value_n = self.ac.v(torch.FloatTensor(s_n))
        
        q_target = rew + self.gamma * (1 - done) * value_n
        
        action = torch.FloatTensor([a])
        _, log_proba = self.ac.pi(state, action)
        
        loss_A = - log_proba * q_target.detach()
        loss_C = (q_target - value).pow(2)

        self.optimizer_A.zero_grad()
        loss_A.backward()
        self.optimizer_A.step()

        self.optimizer_C.zero_grad()
        loss_C.backward()
        self.optimizer_C.step()

        if done:
            return
        else:
            a_n = self.choose(s_n)
            return a_n
        return a_n
    
    def choose(self, s):
        state = torch.FloatTensor(s)
        a, _, _ = self.ac.step(state)
        return a.item()