from gym.spaces import Discrete, Box
import torch
import torch.nn as nn
import torch.optim as optim
from utils.nets import make_actor


class DeepREINFORCE:
    name = "Deep REINFORCE"

    def __init__(self, obs_space, act_space, lr, gamma, hidden_sizes, seed):
        if seed is not None:
            torch.manual_seed(seed)
        if isinstance(obs_space, Box):
            ds = obs_space.shape[0]
        elif isinstance(obs_space, Discrete):
            ds = obs_space.n
        if isinstance(act_space, Box):
            da = act_space.shape[0]
        elif isinstance(act_space, Discrete):
            da = act_space.n

        self.lr = lr
        self.gamma = gamma

        self.actor = make_actor(ds, da, act_space, hidden_sizes)
        self.optimizer = optim.Adam(self.actor.parameters(), lr=lr)
        self.list_reward = []
        self.log_probas = []

    def reset_episode(self):
        self.list_reward = []
        self.log_probas = []

    def compute_returns(self):
        rewards = self.list_reward
        rewards.reverse()
        G = [rewards[0]]
        G_t = G[0]
        for i in range(1, len(rewards)):
            r = rewards[i]
            G_t = r + self.gamma * G_t
            G.append(G_t)
        G.reverse()
        return G

    def choose(self, s):
        state = torch.FloatTensor(s)
        self.pi = self.actor.distribution(state)
        a = self.pi.sample().item()
        return a

    def step(self, s, a, rew, s_n, done):
        state = torch.FloatTensor(s)
        self.list_reward.append(torch.tensor([rew], dtype=torch.float))
        
        a = torch.tensor(a).unsqueeze(0)
        log_proba = self.pi.log_prob(a)
        self.log_probas.append(log_proba)
        
        if done:
            self.update()
            self.reset_episode()
            return
        else:
            a_n = self.choose(s_n)
            return a_n
    
    def update(self):
        G = self.compute_returns()
        G = torch.cat(G).detach()
        log_probas = torch.cat(self.log_probas)
        
        loss = - (log_probas * G).mean()
        
        self.optimizer.zero_grad()
        loss.backward()
        self.optimizer.step()

