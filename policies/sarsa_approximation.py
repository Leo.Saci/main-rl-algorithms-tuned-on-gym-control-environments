from gym.spaces import Discrete, Box
import numpy as np 
import matplotlib.pyplot as plt

class Sarsa_approximation:
    def __init__(self, obs_space, act_space, epsilon, alpha, gamma, lambd, epsilon_decay=None):
        self.name = 'Sarsa({})'.format(lambd)
        if isinstance(obs_space, Box):
            ds = obs_space.shape[0]
        elif isinstance(obs_space, Discrete):
            ds = obs_space.n
        na = act_space.n

        self.ds = ds
        self.na = na
        self.lambd = lambd
        self.alpha = alpha
        self.epsilon = epsilon
        self.weights = np.zeros(ds * na)
        self.E = np.zeros(ds * na)
        self.gamma = gamma
        self.epsilon_decay = epsilon_decay
    
    def reset_episode(self):
        self.E = np.zeros(self.ds * self.na)
    
    def phi(self, s, a):
        Phi = np.zeros(self.ds * self.na)
        Phi[a*self.ds: (a+1)*self.ds] = s 
        return Phi
    
    def model(self, s, a):
        return self.weights @ self.phi(s, a)
    
    def greedy(self, s):
        a = np.argmax([self.model(s, a) for a in range(self.na)])
        return a
    
    def eps_greedy(self, s):
        if np.random.rand() < self.epsilon:
            a = np.random.randint(self.na)
        else:
            a = self.greedy(s)
        return a
    
    def step(self, s, a, rew, s_n, done):
        a_n = self.choose(s_n)
        self.E = self.lambd * self.gamma * self.E + self.phi(s, a)
        # print(self.E)
        # print()
        if not done:
            delta = rew + self.gamma * self.model(s_n, a_n) - self.model(s, a)
        else:
            delta = rew - self.model(s, a)
            # self.reset_episode()
            if self.epsilon_decay is not None:
                self.epsilon *= self.epsilon_decay
        self.weights = self.weights + self.alpha * delta * self.E
        return a_n 
    
    def choose(self, s_n):
        return self.eps_greedy(s_n)