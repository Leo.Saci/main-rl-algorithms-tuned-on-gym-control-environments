# RL algorithms

### TODO LIST

- [x] choose env in openai gym : FrozenLake-v0, MountainCar-v0
- [x] Sarsa(<img src="https://render.githubusercontent.com/render/math?math=\lambda">) with FrozenLake-v0 (Leo)
- [x] Q learning with FrozenLake-v0 (Armand)
- [x] DQN with MountainCar-v0 with discretized action (Leo)
- [x] REINFORCE with MountainCar-v0 (Armand)
- [x] Q Actor critic (Leo)
- [x] TD(<img src="https://render.githubusercontent.com/render/math?math=\lambda">) /  Advantage Actor Critic with MountainCar-v0 (Armand)


### Instalation requirements

- conda install -c conda-forge gym
- conda install -c pytorch pytorch
- conda install -c conda-forge matplotlib



