import gym
from utils.experiment import experiment

ENV_NAME = "Acrobot-v1"
SCORING = ["rew", "cum_rew", "avg_cum_rew", "runs"][0]
seed = 2

env = gym.make(ENV_NAME)
obs_space = env.observation_space
act_space = env.action_space

if seed is not None:
    env.seed(seed)

dict_list = []
dict_list.append(
    {
        "name": "REINFORCE",
        "lr": 1e-3,
        "gamma": 0.99,
    }
)
dict_list.append(
    {
        "name": "Deep Advantage REINFORCE",
        "lr_A": 1e-3,
        "lr_V": 1e-3,
        "gamma": 0.99,
        "hidden_sizes": (128,)
    }
)
dict_list.append(
    {
        "name": "Deep REINFORCE",
        "lr": 1e-3,
        "gamma": 0.99,
        "hidden_sizes": (128,)
    }
)
dict_list.append(
    {
        "name": "DQN",
        "lr": 1e-3,
        "max_memory": 1000,
        "batch_size": 128,
        "epsilon": 0.1,
        "gamma": 0.99,
        "hidden_sizes": (128,),
        "target_delay": 100
    }
)
dict_list.append(
    {
        "name": "Double DQN",
        "lr": 1e-3,
        "max_memory": 1000,
        "batch_size": 64,
        "epsilon": 0.1,
        "gamma": 0.99,
        "target_delay": 100
    }
)
dict_list.append(
    {
        "name": "Sarsa approximation",
        "epsilon": 0.05,
        "alpha": 1e-1,
        "gamma": 0.99,
        "lambd": 0.5,
        "epsilon_decay": None,
    }
)
dict_list.append(
    {
        "name": "QAC",
        "layers_size": (32,),
        "lr_A": 1e-3,
        "lr_C": 1e-3,
        "gamma": 0.99,
    }
)
dict_list.append(
    {
        "name": "A2C",
        "layers_size": (32,),
        "lr_A": 1e-3,
        "lr_C": 1e-3,
        "gamma": 0.99,
    }
)
dict_list.append(
    {
        "name": "PPO",
        "layers_size": (32,),
        "lr": 1e-3,
        "batch_size": 2000,
        "gamma": 0.99,
        "eps_clip": 0.2,
        "update_interval": 500,
        "n_epochs": 4,
        "estimate_adv": "GAE",
    }
)

grid_search=[]
n_runs = 1
n_ep = 2
seed = seed if n_runs == 1 else [s for s in range(seed, seed + n_runs)]

experiment(env, dict_list, n_runs=n_runs, n_ep=n_ep, env_name=ENV_NAME, scoring=SCORING, render=False, grid_search=grid_search, seed=seed)
